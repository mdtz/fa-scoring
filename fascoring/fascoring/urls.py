from django.conf.urls import url, include
from django.contrib import admin

from players import views as players_views
from players.api.v1 import urls as api_v1_players_urls
from users import urls as user_team_urls


urlpatterns = [
    # welcome home
    url(r'^$', players_views.index),

    # login + social connection
    url(r'^', include('social_django.urls', namespace='social')),
    url(r'^login/$', players_views.login),

    # league details
    url(r'^teams/', include(user_team_urls)),

    # api
    url(r'^api/v1/players/', include(api_v1_players_urls)),

    # admin
    url(r'^admin/', admin.site.urls),
]
