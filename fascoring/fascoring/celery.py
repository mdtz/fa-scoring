"""Celery creation and configuration
"""

from __future__ import absolute_import, unicode_literals
import os

from celery import Celery


# load django settings, turn off debug mode
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fascoring.settings')
import django
django.setup()
from django.conf import settings
settings.DEBUG = False

app = Celery(
    'fascoring',
    broker='redis://localhost:6379',
    backend='redis://localhost:6379',
)

app.autodiscover_tasks(['players'], force=True)
