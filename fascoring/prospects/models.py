from django.db import models

from players.models import Player


UNRANKED_SCORE = 99

class ProspectLine(models.Model):
    player = models.ForeignKey(Player)
    rank_mlbam = models.IntegerField(default=0)
    rank_fg = models.IntegerField(default=0)
    rank_bp = models.IntegerField(default=0)
    rank_ba = models.IntegerField(default=0)
    rank_composite = models.IntegerField(default=0)

    def __str__(self):
        return self.player.name_full

    def save(self, *a, **kw):
        points_mlb = 100 - (self.rank_mlbam or UNRANKED_SCORE)
        points_fg = 100 - (self.rank_fg or UNRANKED_SCORE)
        points_bp = 101 - (self.rank_bp or UNRANKED_SCORE)
        points_ba = 100 - (self.rank_ba or UNRANKED_SCORE)

        # create composite ranking
        self.rank_composite = points_mlb + points_fg + points_bp + points_ba

        super(ProspectLine, self).save(*a, **kw)
