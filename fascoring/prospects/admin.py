from django.contrib import admin

from .models import ProspectLine


@admin.register(ProspectLine)
class ProspectLineAdmin(admin.ModelAdmin):
    list_display = ('player', 'rank_mlbam', 'rank_fg', 'rank_bp', 'rank_ba', )
    list_editable = ('rank_mlbam', 'rank_fg', 'rank_bp', 'rank_ba', )
    search_fields = ('player__name_full', )
