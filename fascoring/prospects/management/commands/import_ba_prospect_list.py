"""
Imports prospect lists
"""

import csv
import os

from django.core.management import BaseCommand

from players.models import Player
from ...models import ProspectLine


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-i', dest='input_path', required=True)

    def handle(self, **opts):
        reader = csv.DictReader(
            open(os.path.realpath(opts['input_path']), 'r'))

        for row in reader:
            prospect = (ProspectLine.objects
                       .filter(player__name_full=row['name'])
                       .first())

            if prospect is None:
                print('Could not fetch prospect {} (#{}, {})'
                      .format(row['name'], row['rank'], row['id']))
                continue

            prospect.rank_ba = int(row['rank'])
            prospect.save(update_fields=('rank_ba', ))
