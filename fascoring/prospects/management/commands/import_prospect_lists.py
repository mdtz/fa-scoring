"""
Imports prospect lists
"""

import csv
import os

from django.core.management import BaseCommand

from players.models import Player
from ...models import ProspectLine


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-b', dest='bp_path',  required=True)
        parser.add_argument('-f', dest='fg_path',  required=True)
        parser.add_argument('-m', dest='mlb_path', required=True)

    def handle(self, **opts):
        missing_writer = csv.writer(open('missing.csv', 'w'))

        bp_reader = csv.DictReader(
            open(os.path.realpath(opts['bp_path']), 'r'))
        fg_reader = csv.DictReader(
            open(os.path.realpath(opts['fg_path']), 'r'))
        mlb_reader = csv.DictReader(
            open(os.path.realpath(opts['mlb_path']), 'r'))

        pairs = (
            (bp_reader,  'key_bpro',  'rank_bp'),
            (fg_reader,  'key_fg',    'rank_fg'),
            (mlb_reader, 'key_mlbam', 'rank_mlbam'),
        )

        for reader, key, field in pairs:
            for row in reader:
                q_kw = {key: row['id']}
                player = Player.objects.filter(**q_kw).first()

                if player is None:
                    print('Could not fetch player {} ({}) by key {}'
                          .format(row['name'], row['id'], key))
                    missing_writer.writerow([key, row['id'], row['name']])
                    continue

                ProspectLine.objects.update_or_create(
                    player=player,
                    defaults={field: row['rank']}
                )
