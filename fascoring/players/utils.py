from lxml import html
from lxml.etree import _ElementUnicodeResult as ElementUnicodeResult

import re


__all__ = ('process_schedule_data', )

TEAM_PREFIX_RE = re.compile(r'\d+ Games \(\d+ Home\)\:')

def clean_team_name(value):
    return TEAM_PREFIX_RE.sub('', value).strip()


def process_schedule_data(value):
    doc = html.fromstring(value)
    raw_schedule_iter = doc.xpath('child::node()')

    schedule = []
    current_stand = {'home': None, 'team': None}

    for node in raw_schedule_iter:
        games = []

        if type(node) is ElementUnicodeResult:
            if node in (',', ''):
                continue
            else:
                teams_in_stand = filter(lambda t: t,
                                        map(lambda t: t.strip(),
                                            node.split(',')))
                for team in teams_in_stand:
                    team = clean_team_name(team)
                    if team != '':
                        games.append({'team': team, 'home': False})
        else:
            games.append({
                'team': clean_team_name(node.text.strip()),
                'home': True
            })

        for game in games:
            if (game['team'] != current_stand['team']
                or game['home'] != current_stand['home']):
                # new series in the schedule
                current_stand = game
                schedule.append({
                    'team': game['team'].upper(),
                    'home': game['home'],
                    'count': 0,
                })

            schedule[-1]['count'] += 1

    return schedule
