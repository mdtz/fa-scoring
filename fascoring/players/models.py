from django.db import models


class Player(models.Model):
    """Describes a single player
    """

    name_last = models.CharField(max_length=60, blank=True, default='')
    name_first = models.CharField(max_length=60, blank=True, default='')
    name_full = models.CharField(max_length=124, default='')
    age = models.IntegerField(blank=True, null=True)
    primary_pos = models.CharField(max_length=3)

    # keys
    key_bpro = models.CharField(max_length=24, blank=True, default='')
    key_mlbam = models.CharField(max_length=24, blank=True, default='')
    key_espn = models.CharField(max_length=24, blank=True, default='')
    key_cbs = models.CharField(max_length=24, blank=True, default='')
    key_yahoo = models.CharField(max_length=24, blank=True, default='')
    key_ottoneu = models.CharField(max_length=24, blank=True, default='')
    key_fg = models.CharField(max_length=24, blank=True, default='')
    key_fg_minors = models.CharField(max_length=24, blank=True, default='')

    # team info
    team_name = models.CharField(max_length=3, default='')
    team_sos = models.FloatField(blank=True, null=True)
    team_upcoming = models.CharField(max_length=124, default='', blank=True)

    # dl details
    dl_eligible = models.BooleanField(default=False)
    dl_status = models.CharField(max_length=32, blank=True, default='')
    dl_reason = models.CharField(max_length=128, blank=True, default='')
    dl_last_updated = models.DateField(blank=True, null=True)

    class Meta:
        ordering = ('name_full', )

    def __str__(self):
        return self.name_full


class Statline(models.Model):
    """Describes a player's stats at a certain league level
    """

    player = models.ForeignKey(Player, related_name='statline')
    level = models.CharField(max_length=3, default='MLB')

    # games played
    pos_c = models.IntegerField(default=0)
    pos_1b = models.IntegerField(default=0)
    pos_2b = models.IntegerField(default=0)
    pos_ss = models.IntegerField(default=0)
    pos_3b = models.IntegerField(default=0)
    pos_lf = models.IntegerField(default=0)
    pos_cf = models.IntegerField(default=0)
    pos_rf = models.IntegerField(default=0)
    pos_dh = models.IntegerField(default=0)
    pos_sp = models.IntegerField(default=0) # this duplicates games + started
    pos_rp = models.IntegerField(default=0) # ... but that's ok for now

    # from bp
    b_pa = models.IntegerField(blank=True, null=True)
    b_ab = models.FloatField(blank=True, null=True)
    b_avg = models.FloatField(blank=True, null=True)
    b_iso = models.FloatField(blank=True, null=True)
    b_hr = models.IntegerField(blank=True, null=True)
    b_sb = models.IntegerField(blank=True, null=True)
    b_cs = models.IntegerField(blank=True, null=True)
    b_rbi = models.IntegerField(blank=True, null=True)
    b_r = models.IntegerField(blank=True, null=True)
    b_leadoff_pa = models.IntegerField(blank=True, null=True)
    b_obp = models.FloatField(blank=True, null=True)
    b_slg = models.FloatField(blank=True, null=True)
    b_k = models.IntegerField(blank=True, null=True)
    b_so_bb = models.FloatField(blank=True, null=True)
    b_tavg_minus_avg = models.FloatField(blank=True, null=True)
    b_tavg = models.FloatField(blank=True, null=True)
    b_fb_pct = models.FloatField(blank=True, null=True)
    b_gb_pct = models.FloatField(blank=True, null=True)
    b_ld_pct = models.FloatField(blank=True, null=True)
    b_babip = models.FloatField(blank=True, null=True)
    b_rookie = models.BooleanField(default=False)
    b_woba = models.FloatField(blank=True, null=True)
    b_wrc_plus = models.FloatField(blank=True, null=True)
    b_pull_pct = models.FloatField(blank=True, null=True)
    b_cent_pct = models.FloatField(blank=True, null=True)
    b_oppo_pct = models.FloatField(blank=True, null=True)
    b_oswing_pct = models.FloatField(blank=True, null=True)
    b_zswing_pct = models.FloatField(blank=True, null=True)
    b_swing_pct = models.FloatField(blank=True, null=True)
    b_ocontact_pct = models.FloatField(blank=True, null=True)
    b_zcontact_pct = models.FloatField(blank=True, null=True)
    b_contact_pct = models.FloatField(blank=True, null=True)
    b_zone_pct = models.FloatField(blank=True, null=True)
    b_fstrike_pct = models.FloatField(blank=True, null=True)
    b_swstr_pct = models.FloatField(blank=True, null=True)
    b_wpa_li = models.FloatField(blank=True, null=True)

    # sp-specific fields
    p_next_start_date = models.DateField(blank=True, null=True)

    # stats
    p_g = models.IntegerField(blank=True, null=True)
    p_gs = models.IntegerField(blank=True, null=True)
    p_losses = models.IntegerField(blank=True, null=True)
    p_wins = models.IntegerField(blank=True, null=True)
    p_pa = models.IntegerField(blank=True, null=True)
    p_ab = models.IntegerField(blank=True, null=True)
    p_k_9 = models.FloatField(blank=True, null=True)
    p_bb_9 = models.FloatField(blank=True, null=True)
    p_so_bb = models.FloatField(blank=True, null=True)
    p_hld = models.IntegerField(blank=True, null=True)
    p_whip = models.FloatField(blank=True, null=True)
    p_sv = models.IntegerField(blank=True, null=True)
    p_qs = models.IntegerField(blank=True, null=True)
    p_dra = models.FloatField(blank=True, null=True)
    p_dra_minus = models.IntegerField(blank=True, null=True)
    p_cfip = models.FloatField(blank=True, null=True)
    p_fip = models.FloatField(blank=True, null=True)
    p_era = models.FloatField(blank=True, null=True)
    p_ip = models.FloatField(blank=True, null=True)
    p_ip_start = models.FloatField(blank=True, null=True)
    p_ip_relief = models.FloatField(blank=True, null=True)
    p_babip = models.FloatField(blank=True, null=True)
    p_gb_fb = models.FloatField(blank=True, null=True)
    p_hr_fb = models.FloatField(blank=True, null=True)
    p_gmli = models.FloatField(blank=True, null=True)
    p_wpa_li = models.FloatField(blank=True, null=True)
    p_fstrike_pct = models.FloatField(blank=True, null=True)
    p_swstr_pct = models.FloatField(blank=True, null=True)
    p_soft_pct = models.FloatField(blank=True, null=True)
    p_med_pct = models.FloatField(blank=True, null=True)
    p_hard_pct = models.FloatField(blank=True, null=True)

    # xstats
    p_x_xwoba_plus = models.FloatField(blank=True, null=True) # xwoba, all
    p_x_rh_xwoba_plus = models.FloatField(blank=True, null=True) # xwoba, rhh
    p_x_lh_xwoba_plus = models.FloatField(blank=True, null=True) # xwoba, lhh
    p_x_avg_ev = models.FloatField(blank=True, null=True) # exit vel, all
    p_x_rh_avg_ev = models.FloatField(blank=True, null=True) # exit vel, rhh
    p_x_lh_avg_ev = models.FloatField(blank=True, null=True) # exit vel, lhh
    p_x_vh = models.FloatField(blank=True, null=True) # hard hit balls, all
    p_x_rh_vh = models.FloatField(blank=True, null=True) # hard hit balls, lhh
    p_x_lh_vh = models.FloatField(blank=True, null=True) # hard hit balls, rhh

    class Meta:
        unique_together = (('player', 'level', ), )

    def __str__(self):
        return '%s @ %s' % (self.player.name_full, self.level)
