from django.contrib import admin

from .models import Player


class EmptyIdFilter(admin.SimpleListFilter):
    title = 'Missing an ID'
    parameter_name = 'system'

    def lookups(self, request, model_admin):
        return (
            ('key_mlbam', 'MLB'),
            ('key_bpro', 'BP'),
            ('key_espn', 'ESPN'),
            ('key_fg', 'Fangraphs'),
            ('key_cbs', 'CBS'),
            ('key_yahoo', 'Yahoo'),
            ('key_ottoneu', 'Ottoneu'),
        )

    def queryset(self, request, queryset):
        value = self.value()

        if not value:
            return queryset

        kw = {value: ''}
        return queryset.filter(**kw)


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ('name_full', 'team_name', 'primary_pos',
                    'key_bpro', 'key_mlbam', 'key_espn',
                    'key_cbs', 'key_yahoo', 'key_ottoneu',
                    'key_fg', )
    list_filter = (EmptyIdFilter, 'team_name', 'primary_pos')
    list_editable = ('key_bpro', 'key_mlbam', 'key_espn',
                    'key_cbs', 'key_yahoo', 'key_ottoneu',
                    'key_fg', )
    search_fields = ('name_full', )
