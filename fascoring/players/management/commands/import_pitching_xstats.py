import csv
import io
import json

from django.core.management import BaseCommand

import requests

from ...models import Player, Statline


XSTATS_URL = 'https://spreadsheets.google.com/feeds/worksheets/1Rn6YLAyKkZ1-Uaoi02ZPWsldTchhF5tm377i2cxxHh0/public/basic'

FIELDS = [
    # - everyone -
    'name',
    'key_fangraphs',
    'team_name',
    'throws',
    'ip',
    'tbf',
    'ab',
    'scfip',
    'fip',
    'xavg',
    'xobp',
    'xslg',
    'xbabip',
    'xbacon',
    'xoba',
    'xoba+',
    'x1b',
    'x2b',
    'x3b',
    'xhr',
    'vh%',
    'ph%',
    'k%',
    'outs',
    'avg_total',
    'avg_exit_velocity',
    'avg_vertical',
    'avg_spray',
    'avg_db%',
    'avg_gb%',
    'avg_ld%',
    'avg_hd%',
    'avg_fb%',
    'avg_pu%',

    # dribble hits
    'db_total',
    'db_avg_exit_velocity',
    'db_vertical',
    'db_spray',

    # groundballs
    'gb_total',
    'gb_avg_exit_velocity',
    'gb_vertical',
    'gb_spray',

    # low drives
    'ld_total',
    'ld_avg_exit_velocity',
    'ld_vertical',
    'ld_spray',

    # high drives
    'hd_total',
    'hd_avg_exit_velocity',
    'hd_vertical',
    'hd_spray',

     # fly balls
    'fb_total',
    'fb_avg_exit_velocity',
    'fb_vertical',
    'fb_spray',

     # pop-ups
    'pu_total',
    'pu_avg_exit_velocity',
    'pu_vertical',
    'pu_spray',

    # - righties -
    'rh_tbf',
    'rh_ab',
    'rh_xavg',
    'rh_xobp',
    'rh_xslg',
    'rh_xbabip',
    'rh_xbacon',
    'rh_xoba',
    'rh_xoba+',
    'rh_x1b',
    'rh_x2b',
    'rh_x3b',
    'rh_xhr',
    'rh_vh%',
    'rh_ph%',
    'rh_k%',
    'rh_outs',
    'rh_avg_total',
    'rh_avg_exit_velocity',
    'rh_avg_vertical',
    'rh_avg_spray',
    'rh_avg_db%',
    'rh_avg_gb%',
    'rh_avg_ld%',
    'rh_avg_hd%',
    'rh_avg_fb%',
    'rh_avg_pu%',

    # dribble hits
    'rh_db_total',
    'rh_db_avg_exit_velocity',
    'rh_db_vertical',
    'rh_db_spray',

    # groundballs
    'rh_gb_total',
    'rh_gb_avg_exit_velocity',
    'rh_gb_vertical',
    'rh_gb_spray',

    # low drives
    'rh_ld_total',
    'rh_ld_avg_exit_velocity',
    'rh_ld_vertical',
    'rh_ld_spray',

    # high drives
    'rh_hd_total',
    'rh_hd_avg_exit_velocity',
    'rh_hd_vertical',
    'rh_hd_spray',

     # fly balls
    'rh_fb_total',
    'rh_fb_avg_exit_velocity',
    'rh_fb_vertical',
    'rh_fb_spray',

     # pop-ups
    'rh_pu_total',
    'rh_pu_avg_exit_velocity',
    'rh_pu_vertical',
    'rh_pu_spray',

    # - lefties -
    'lh_tbf',
    'lh_ab',
    'lh_xavg',
    'lh_xobp',
    'lh_xslg',
    'lh_xbabip',
    'lh_xbacon',
    'lh_xoba',
    'lh_xoba+',
    'lh_x1b',
    'lh_x2b',
    'lh_x3b',
    'lh_xhr',
    'lh_vh%',
    'lh_ph%',
    'lh_k%',
    'lh_outs',
    'lh_avg_total',
    'lh_avg_exit_velocity',
    'lh_avg_vertical',
    'lh_avg_spray',
    'lh_avg_db%',
    'lh_avg_gb%',
    'lh_avg_ld%',
    'lh_avg_hd%',
    'lh_avg_fb%',
    'lh_avg_pu%',

    # dribble hits
    'lh_db_total',
    'lh_db_avg_exit_velocity',
    'lh_db_vertical',
    'lh_db_spray',

    # groundballs
    'lh_gb_total',
    'lh_gb_avg_exit_velocity',
    'lh_gb_vertical',
    'lh_gb_spray',

    # low drives
    'lh_ld_total',
    'lh_ld_avg_exit_velocity',
    'lh_ld_vertical',
    'lh_ld_spray',

    # high drives
    'lh_hd_total',
    'lh_hd_avg_exit_velocity',
    'lh_hd_vertical',
    'lh_hd_spray',

     # fly balls
    'lh_fb_total',
    'lh_fb_avg_exit_velocity',
    'lh_fb_vertical',
    'lh_fb_spray',

     # pop-ups
    'lh_pu_total',
    'lh_pu_avg_exit_velocity',
    'lh_pu_vertical',
    'lh_pu_spray',
]

class Command(BaseCommand):
    def handle(self, **opts):
        resp = requests.get(XSTATS_URL, params={'alt': 'json'})
        body = resp.json()

        def get_worksheet_url_by_title(worksheets, title):
            for worksheet in worksheets:
                if worksheet['title']['$t'] == title:
                    url = filter(lambda l: l['type'] == 'text/csv', worksheet['link'])
                    url = list(url)
                    if len(url) > 0:
                        return url[0]['href']

            raise Exception('Cannot find text/csv copy in rels')

        pitcher_sheet_url = get_worksheet_url_by_title(body['feed']['entry'],
                                                       'P17')
        resp = requests.get(pitcher_sheet_url)
        buff = io.StringIO(resp.text)
        reader = csv.reader(buff)

        # advance the reader beyond meaningless rows
        for i in range(0, 5):
            next(reader)

        for row in reader:
            x_row = dict(zip(FIELDS, row))

            updates = {
                'p_x_xwoba_plus': x_row['xoba+'],
                'p_x_rh_xwoba_plus': x_row['rh_xoba+'],
                'p_x_lh_xwoba_plus': x_row['lh_xoba+'],
                'p_x_avg_ev': x_row['avg_exit_velocity'],
                'p_x_rh_avg_ev': x_row['rh_avg_exit_velocity'],
                'p_x_lh_avg_ev': x_row['lh_avg_exit_velocity'],
                'p_x_vh': x_row['vh%'].replace('%', '').strip(),
                'p_x_rh_vh': x_row['rh_vh%'].replace('%', '').strip(),
                'p_x_lh_vh': x_row['lh_vh%'].replace('%', '').strip(),
            }

            player = (Player.objects
                      .filter(key_fg=x_row['key_fangraphs'])
                      .first())

            (Statline.objects
             .filter(player=player, level='MLB')
             .update(**updates))
