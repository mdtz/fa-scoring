"""
Import Fangraphs data file
"""

import csv
import io

import requests
from lxml import html

from django.core.management import BaseCommand

from ...models import Player, Statline


BASE_URL = 'http://www.fangraphs.com/leaders.aspx?pos=all&stats=bat&lg={level_id}&qual=0&type=c,42,50,61,206,207,208,102,103,104,105,106,107,108,109,110,70,5,6&season=2017&month=0&season1=2017&ind=1&team=&rost=&age=&filter=&players='

class Command(BaseCommand):
    def handle(self, **opts):
        levels = [
            ['A-',  [12, 13]],
            ['A',   [11, 14]],
            ['A+',  [8, 9, 10]],
            ['AA',  [5, 6, 7]],
            ['AAA', [2, 4, 31]],
            ['MLB', ['all']],
        ]

        for level, level_ids in levels:
            print(level)

            for level_id in level_ids:
                url = BASE_URL.format(level_id=level_id)
                resp = requests.get(url)
                doc = html.fromstring(resp.content)

                # extract form from page
                forms = doc.xpath(
                    '//form[contains(@action, "leaders.aspx?")]')
                form = forms[0]

                form_data = {
                    '__EVENTTARGET': 'LeaderBoard1$cmdCSV',
                }

                for hidden_input in form.findall('./input[@type="hidden"]'):
                    value = hidden_input.attrib['value']
                    form_data[hidden_input.attrib['name']] = value

                # fetch data, correct for oddball unicode byte marker
                resp = requests.post(url, data=form_data)
                corrected = resp.text.replace('\ufeff"Name"', '"Name"')

                # buffer, throw into a csv reader
                buff = io.StringIO(corrected)
                reader = csv.DictReader(buff)

                for row in reader:
                    defaults = {
                        'b_wrc_plus': row['wRC+'] or None,
                        'b_woba': row['wOBA'] or None,
                        'b_pull_pct': row['Pull%'].replace('%', '') or None,
                        'b_cent_pct': row['Cent%'].replace('%', '') or None,
                        'b_oppo_pct': row['Oppo%'].replace('%', '') or None,
                        'b_oswing_pct': row['O-Swing%'].replace('%', '') or None,
                        'b_zswing_pct': row['Z-Swing%'].replace('%', '') or None,
                        'b_swing_pct': row['Swing%'].replace('%', '') or None,
                        'b_ocontact_pct': row['O-Contact%'].replace('%', '') or None,
                        'b_zcontact_pct': row['Z-Contact%'].replace('%', '') or None,
                        'b_contact_pct': row['Contact%'].replace('%', '') or None,
                        'b_zone_pct': row['Zone%'].replace('%', '') or None,
                        'b_fstrike_pct': row['F-Strike%'].replace(' %', '') or None,
                        'b_swstr_pct': row['SwStr%'].replace(' %', '') or None,
                        'b_wpa_li': row['SwStr%'].replace(' %', '') or None,
                    }

                    player = Player.objects.filter(key_fg=row['playerid']).first()

                    if player is None:
                        print('Cannot find player by Fangraphs id: {} {}'
                              .format(row['Name'], row['playerid']))
                        continue

                    (Statline.objects
                     .filter(player=player, level=level)
                     .update(**defaults))
