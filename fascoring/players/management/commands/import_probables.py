"""
Imports probable pitchers from MLBAM's site for yesterday thru +1 week
"""

import datetime

from django.core.management import BaseCommand

from lxml import html

import requests

from ...models import Player, Statline


PROBABLES_URL = 'http://mlb.mlb.com/news/probable_pitchers/?c_id=mlb&date={date}'

class Command(BaseCommand):
    def handle(self, **opts):
        today = datetime.date.today()
        date_range = [today + datetime.timedelta(days=i) for i in range(0, 5)]

        Statline.objects.filter().update(p_next_start_date=None)

        for date in date_range:
            url = PROBABLES_URL.format(date=date.strftime('%Y/%m/%d'))

            # fetch injury data, parse page
            resp = requests.get(url)

            if resp.status_code != requests.codes.ok:
                print('Could not fetch probables for {}'.format(date))
                continue

            doc = html.fromstring(resp.content)
            pitchers = doc.xpath(
                '//div[contains(@class, "pitcher") and string-length(@pid)]')

            for pitcher in pitchers:
                is_home = 'last' in pitcher.attrib['class']

                player = (Player.objects
                          .filter(key_mlbam=pitcher.attrib['pid'])
                          .first())

                if player is None:
                    print('Could not set next start date for {}'
                          .format(pitcher.attrib['pid']))
                    continue

                (Statline.objects
                 .filter(player=player, level='MLB')
                 .update(p_next_start_date=date))
