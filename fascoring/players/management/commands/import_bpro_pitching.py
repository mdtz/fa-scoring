"""
Import Baseball Prospectus report
"""

import csv
import io
import os

from django.core.management import BaseCommand

import requests

from ...models import Player, Statline


class Command(BaseCommand):
    def handle(self, **opts):
        levels = ('A', 'A-', 'A+', 'AA', 'AAA', 'MLB')

        for level in levels:
            print(level)

            resp = requests.post(
                'http://www.baseballprospectus.com/sortable/index.php',
                data={
                    'mystatslist': 'LVL,NAME,YEAR,TEAMS,AGE,IP,PA,FIP,CFIP,DRA,DRA_MINUS,IP_RELIEF,IP_START,W,L,AB,SO,SO_BB,SO9,BB9,HD,WHIP,SV,QS,ERA,G,GS,ERA,PITCHER,POS',
                    'category': 'pitcher_season',
                    'tablename': 'dyna_pitcher_season',
                    'stage':'data',
                    'year':2017,
                    'group_LVL': level,
                    'minimum': 0,
                    'sort1column': 'LVL',
                    'sort1order': 'ASC',
                    'sort2column': 'LVL',
                    'sort2order': 'ASC',
                    'sort3column': 'LVL',
                    'sort3order': 'ASC',
                    'page_limit': 30,
                    'glossary_terms': '*',
                    'tt_team': '*',
                    'show_ttroster': 1,
                    'show_ttwatched': 1,
                    'csvsubmit': 'CSV',
                })

            # read response into buffered io, then parse csv
            buf = io.StringIO(resp.text)
            reader = csv.DictReader(buf)

            for row in reader:
                current_team = row['TEAMS'].split(',')[-1].strip()

                player_defaults = {
                    'name_full': row['NAME'],
                    'team_name': current_team,
                    'age': row['AGE'],
                    # 'primary_pos': row['POS'],
                }

                statline_defaults = {
                    'p_ip': row['IP'].replace('.3', '.1').replace('.7', '.2'),
                    'p_dra': row['DRA'] or None,
                    'p_dra_minus': row['DRA-'] or None,
                    'p_fip': row['FIP'] or None,
                    'p_era': row['ERA'] or None,
                    'p_cfip': row['cFIP'] or None,
                    'p_ip_start': row['IP Start'] or None,
                    'p_ip_relief': row['IP Relief'] or None,
                    'p_wins': row['W'] or None,
                    'p_losses': row['L'] or None,
                    'p_pa': row['PA'] or None,
                    'p_ab': row['AB'] or None,
                    'p_k_9': row['SO9'] or None,
                    'p_bb_9': row['BB9'] or None,
                    'p_so_bb': row['SO/BB'] or None,
                    'p_hld': row['Hold'] or None,
                    'p_whip': row['WHIP'] or None,
                    'p_sv': row['SV'] or None,
                    'p_qs': row['QS'] or None,
                    'p_g': row['G'] or None,
                    'p_gs': row['GS'] or None,
                    'level': level,
                }

                # update player
                player, _ = Player.objects.update_or_create(
                    key_bpro=row['PITCHER'],
                    defaults=player_defaults,
                )

                # update player's stats
                statline, _ = Statline.objects.update_or_create(
                    player=player,
                    level=level,
                    defaults=statline_defaults
                )
