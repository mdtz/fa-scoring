"""
Fangraphs changes player IDs when a prospect transitions into the majors.
This script checks all players whose Fangraphs ID begins with `sa`,
and updates their ID internally if a match is found.
"""

import re

import requests

from django.core.management import BaseCommand

from players.models import Player


BASE_URL = 'http://www.fangraphs.com/statss.aspx?playerid={fg_id}'
PLAYER_ID_RE = re.compile(r'playerid=(\d+)')

class Command(BaseCommand):
    def handle(self, **opts):
        fg_minors = Player.objects.exclude(key_fg_minors='')

        print('Found {} player(s)'.format(fg_minors.count()))

        for player in fg_minors:
            url = BASE_URL.format(fg_id=player.key_fg_minors)
            resp = requests.head(url)

            if not player.key_fg_minors in resp.headers['location']:
                match = PLAYER_ID_RE.search(resp.headers['location'])
                if match is not None:
                    new_id = match.group(1)
                    player.key_fg_minors = new_id
                    player.save(update_fields=('key_fg_minors', ))
                    print('Updated key for {}'.format(player))
