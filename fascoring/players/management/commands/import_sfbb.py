"""
Import Crunchtime data file
"""

import collections
import csv
import io
import os

from django.core.management import BaseCommand

import requests

from ...models import Player


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-i', dest='input_file')

    def handle(self, **opts):
        if opts['input_file']:
            input_file = os.path.abspath(opts['input_file'])
            reader = csv.DictReader(open(input_file, 'r'))
        else:
            resp = requests.get('http://www.smartfantasybaseball.com/PLAYERIDMAPCSV')
            buff = io.StringIO(resp.text)
            reader = csv.DictReader(buff)

        for row in reader:
            bp_key = row['BPID'].strip()

            if not bp_key:
                print('Player {} ({}) is missing BP id'
                      .format(row['PLAYERNAME'], row['IDPLAYER']))
                continue

            player = Player.objects.filter(key_bpro=bp_key).first()

            if player is None:
                print('Cannot find player by BP id: {} {}'
                      .format(row['PLAYERNAME'], bp_key))
                continue

            if not player.key_mlbam:
                player.key_mlbam = row['MLBID']

            if not player.key_espn:
                player.key_espn = row['ESPNID']

            if not player.key_cbs:
                player.key_cbs = row['CBSID']

            if not player.key_yahoo:
                player.key_yahoo = row['YAHOOID']

            if not player.key_ottoneu:
                player.key_ottoneu = row['OTTONEUID']

            # if not player.key_fg:
            #     player.key_fg = row['IDFANGRAPHS']

            if not player.key_fg:
                fg_id = row.get('IDFANGRAPHS', '')
                if not fg_id.startswith('sa'):
                    player.key_fg = fg_id

            if not player.key_fg_minors:
                fg_id = row.get('IDFANGRAPHS', '')
                if fg_id.startswith('sa'):
                    player.key_fg_minors = fg_id

            print('Updating player {}'.format(player))

            player.save()
