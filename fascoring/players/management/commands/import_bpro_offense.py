"""
Import Baseball Prospectus report
"""

import csv
import io
import os

from django.core.management import BaseCommand

import requests

from ...models import Player, Statline


class Command(BaseCommand):
    def handle(self, **opts):
        levels = ('A', 'A-', 'A+', 'AA', 'AAA', 'MLB')

        for level in levels:
            print(level)

            resp = requests.post(
                'http://www.baseballprospectus.com/sortable/index.php',
                data={
                    'mystatslist': 'BATTER,NAME,YEAR,LVL,AGE,AVG,ISO,LEADOFF_PA,OBP,SB,CS,SLG,SO,SO_BB,TAV_MINUS_AVG,EQA,TEAMS,THROWS,FB_PERCENT,GB_PERCENT,LINEDR_PERCENT,BABIP,HR,ROOKIE,R,RBI,PA,AB,POS',
                    'category': 'batter_season',
                    'tablename': 'dyna_batter_season',
                    'stage': 'data',
                    'year': 2017,
                    'group_LVL': level,
                    'minimum': 0,
                    'sort1column': 'LVL',
                    'sort1order': 'ASC',
                    'sort2column': 'LVL',
                    'sort2order': 'ASC',
                    'sort3column': 'LVL',
                    'sort3order': 'ASC',
                    'page_limit': 30,
                    'glossary_terms': '*',
                    'tt_team': '*',
                    'show_ttroster': 1,
                    'show_ttwatched': 1,
                    'csvsubmit': 'CSV',
                })

            # read response into buffered io, then parse csv
            buf = io.StringIO(resp.text)
            reader = csv.DictReader(buf)

            for row in reader:
                current_team = row['TEAMS'].split(',')[-1].strip()

                player_defaults = {
                    'name_full': row['NAME'],
                    'team_name': current_team,
                    'age': row['AGE'],
                    'primary_pos': row.get('POS', ''),
                }

                statline_defaults = {
                    'level': level,
                    'b_pa': row['PA'] or None,
                    'b_ab': row['AB'] or None,
                    'b_avg': row['AVG'] or None,
                    'b_iso': row['ISO'] or None,
                    'b_hr': row['HR'] or None,
                    'b_sb': row['SB'] or None,
                    'b_cs': row['CS'] or None,
                    'b_rbi': row['RBI'] or None,
                    'b_r': row['R'] or None,
                    'b_leadoff_pa': row['LEADOFF_PA'] or None,
                    'b_obp': row['OBP'] or None,
                    'b_slg': row['SLG'] or None,
                    'b_k': row['SO'] or None,
                    'b_so_bb': row['SO/BB'] or None,
                    'b_tavg_minus_avg': row['TAV_MINUS_AVG'] or None,
                    'b_tavg': row['TAv'] or None,
                    'b_fb_pct': row['FB%'] or None,
                    'b_gb_pct': row['GB%'] or None,
                    'b_ld_pct': row['LD%'] or None,
                    'b_babip': row['BABIP'] or None,
                    'b_rookie': row['ROOKIE'] or False,
                }

                # update player
                player, _ = Player.objects.update_or_create(
                    key_bpro=row['BATTER'],
                    defaults=player_defaults,
                )

                # update player's stats
                statline, _ = Statline.objects.update_or_create(
                    player=player,
                    level=level,
                    defaults=statline_defaults
                )
