"""
Import Crunchtime data file
"""

import collections
import csv
import io
import os

from django.core.management import BaseCommand

import requests

from ...models import Player


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-i', dest='input_file')

    def handle(self, **opts):
        if opts['input_file']:
            input_file = os.path.abspath(opts['input_file'])
            reader = csv.DictReader(open(input_file, 'r'))
        else:
            resp = requests.get('http://crunchtimebaseball.com/master.csv')
            buff = io.StringIO(resp.text)
            reader = csv.DictReader(buff)

        for row in reader:
            player = Player.objects.filter(key_bpro=row['bp_id']).first()

            if player is None:
                print('Cannot find player by BP id: {} {}'
                      .format(row['mlb_name'], row['bp_id']))
                continue

            if not player.primary_pos:
                all_positions = (
                    row['mlb_pos'],
                    row['cbs_pos'],
                    row['espn_pos'],
                    row['yahoo_pos'],
                    row['ottoneu_pos'],
                )

                positions, _ = (collections.Counter(all_positions)
                                .most_common(1)[0])
                primary_pos = positions.split('/')[0].strip()

                if primary_pos == 'Util':
                    primary_pos = 'U'

                player.primary_pos = primary_pos

            if not player.key_mlbam:
                player.key_mlbam = row['mlb_id']

            if not player.key_espn:
                player.key_espn = row['espn_id']

            if not player.key_cbs:
                player.key_cbs = row['cbs_id']

            if not player.key_yahoo:
                player.key_yahoo = row['yahoo_id']

            if not player.key_ottoneu:
                player.key_ottoneu = row['ottoneu_id']

            if not player.key_fg:
                fg_id = row.get('fg_id', '')
                if not fg_id.startswith('sa'):
                    player.key_fg = fg_id

            if not player.key_fg_minors:
                fg_id = row.get('fg_id', '')
                if fg_id.startswith('sa'):
                    player.key_fg_minors = fg_id

            player.save()
