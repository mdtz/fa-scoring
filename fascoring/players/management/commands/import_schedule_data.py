"""
Import BP's strength of schedule report
"""

import os

from django.core.management import BaseCommand

from lxml import html

import requests

from ...models import Player
from ...utils import process_schedule_data


class Command(BaseCommand):
    def handle(self, **opts):
        # fetch injury data, parse page
        resp = requests.get('http://www.baseballprospectus.com/sortable/extras/strength_of_schedule.php')
        doc = html.fromstring(resp.content)

        # extract team schedules from page
        team_schedules = doc.xpath(
            '//table[@id="pitchers_list_datagrid"]/tbody/tr')
        if len(team_schedules) == 0:
            exit('Cannot find schedule table')

        for schedule in team_schedules:
            team = schedule.find('./td[1]').text.strip()
            rating = schedule.find('./td[2]').text.strip()

            # extract and clean raw schedule string
            raw_schedule = html.tostring(schedule.find('./td[3]'))
            schedule = process_schedule_data(raw_schedule)

            upcoming_desc = []
            for stand in schedule[:3]:
                prefix = '@' if stand['home'] is False else ''
                upcoming_desc.append('{}{}'.format(prefix, stand['team']))
            upcoming_desc = ', '.join(upcoming_desc)

            # set upcoming schedule for team players
            (Player.objects
             .filter(team_name=team)
             .update(team_sos=rating, team_upcoming=upcoming_desc))
