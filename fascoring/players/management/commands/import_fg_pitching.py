"""
Import Fangraphs data file
"""

import csv
import io

import requests
from lxml import html

from django.core.management import BaseCommand
from django.db.models import Q

from ...models import Player, Statline


BASE_URL = 'http://www.fangraphs.com/leaders.aspx?pos=all&stats=pit&lg={level_id}&qual=10&type=c,70,43,46,51,73,112,113,221,222,223&season=2017&month=0&season1=2017&ind=0&team=&rost=&age=&filter=&players='

class Command(BaseCommand):
    def handle(self, **opts):
        levels = [
            ['A-',  [12, 13]],
            ['A',   [11, 14]],
            ['A+',  [8, 9, 10]],
            ['AA',  [5, 6, 7]],
            ['AAA', [2, 4, 31]],
            ['MLB', ['all']],
        ]

        for level, level_ids in levels:
            print(level)

            for level_id in level_ids:
                url = BASE_URL.format(level_id=level_id)
                resp = requests.get(url)
                doc = html.fromstring(resp.content)

                form = doc.xpath('//form[contains(@action, "leaders.aspx?")]')[0]

                form_data = {
                    '__EVENTTARGET': 'LeaderBoard1$cmdCSV',
                }

                for hidden_input in form.findall('./input[@type="hidden"]'):
                    form_data[hidden_input.attrib['name']] = hidden_input.attrib['value']

                # fetch data, correct for oddball unicode byte marker
                resp = requests.post(url, data=form_data)
                corrected = resp.text.replace('\ufeff"Name"', '"Name"')

                # buffer, throw into a csv reader
                buff = io.StringIO(corrected)
                reader = csv.DictReader(buff)

                for row in reader:
                    updates = {
                        'p_babip': row['BABIP'] or None,
                        'p_gb_fb': row['GB/FB'] or None,
                        'p_hr_fb': row['HR/FB'].replace('%', '') or None,
                        'p_gmli': row['gmLI'] or None,
                        'p_wpa_li': row['WPA/LI'] or None,
                        'p_fstrike_pct': row['F-Strike%'].replace('%', '') or None,
                        'p_swstr_pct': row['SwStr%'].replace('%', '') or None,
                        'p_soft_pct': row['Soft%'].replace('%', '') or None,
                        'p_med_pct': row['Med%'].replace('%', '') or None,
                        'p_hard_pct': row['Hard%'].replace('%', '') or None,
                    }

                    player = (Player.objects
                              .filter(Q(key_fg=row['playerid'])
                                      | Q(key_fg_minors=row['playerid']))
                              .first())

                    if player is None:
                        print('Cannot find player by Fangraphs id: {} {}'
                              .format(row['Name'], row['playerid']))
                        continue

                    (Statline.objects
                     .filter(player=player, level=level)
                     .update(**updates))
