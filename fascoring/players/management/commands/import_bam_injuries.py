"""
Import MLB's injury list
"""

import datetime
import os

from django.core.management import BaseCommand

import requests

from ...models import Player


class Command(BaseCommand):
    def handle(self, **opts):
        # fetch injury data
        resp = requests.get('http://mlb.mlb.com/fantasylookup/json/named.wsfb_news_injury.bam')
        body = resp.json()
        injuries = body['wsfb_news_injury']['queryResults']['row']

        # reset dl status for all players
        Player.objects.filter().update(
            dl_eligible=False,
            dl_status='',
            dl_reason='',
            dl_last_updated=None
        )

        for injury in injuries:
            player = (Player.objects
                      .filter(key_mlbam=injury['player_id'])
                      .first())

            if not player:
                print('Not tracking player {}'.format(injury['player_id']))
                continue

            year = datetime.date.today().year
            injury_last_updated = (datetime.datetime
                                   .strptime(injury['insert_ts'], '%M/%d')
                                   .strftime('{}-%M-%d'.format(year)))

            # update player injury status
            player.dl_eligible = True
            player.dl_reason = injury['injury_desc']
            player.dl_status = injury['injury_status']
            player.dl_last_updated = injury_last_updated
            player.save(update_fields=('dl_eligible', 'dl_reason',
                                       'dl_status', 'dl_last_updated'))

            print('Updated DL info for {}'.format(player))
