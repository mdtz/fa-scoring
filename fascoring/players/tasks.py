import datetime

from django.template.loader import render_to_string

import requests

from fascoring.celery import app
from platform_backends.cbs import extract as extract_cbs
from players.models import Player


@app.task
def scrape_platform():
    """Triggers scraping a user's platform

    TODO: wire up real database functionality
    """

    # defer to cbs
    scrape_cbs.delay()


@app.task
def scrape_yahoo(league_id):
    """Triggers scraping a user's Yahoo league
    """

    player_ids = extract_yahoo(league_id)

    players = (Player.objects
               .filter(key_yahoo__in=player_ids,
                       ip__gte=5,
                       dra_minus__lte=100))


@app.task
def scrape_cbs(league_name):
    now = datetime.datetime.now().strftime('%c')

    player_ids = extract_cbs(league_name)

    # fetch pitchers whose ip >= 5
    players = (Player.objects
               .filter(key_cbs__in=player_ids,
                       ip__gte=5,
                       dra_minus__lte=100)
               .order_by('dra', 'cfip'))

    # render email
    html = render_to_string('pitchers.html', {'players': players})

    # send email
    resp = requests.post(
        'https://api.mailgun.net/v3/newsletter.fantasyoverload.com/messages',
        auth=('api', 'key-8d4uhgziuet6sjusap3y76r3-ve0byc5'),
        data={
            'to': 'mattdennewitz@gmail.com',
            'from': 'Fantasy Overflow <mailgun@newsletter.fantasyoverload.com>',
            'html': html,
            'text': html,
            'subject': '⚾ Waiver Wire Pitching Update, {} Ed.'.format(now),
        })

    if not resp.status_code == requests.codes.ok:
        raise Exception('Could not send email: [{}] {}'
                        .format(resp.status_code, resp.text))

    return True
