import json

from django.core.cache import cache
from django.shortcuts import redirect, render
from django.urls import reverse

from platform_backends.cbs import extract_players_for_league as extract_cbs
from players.api.v1.serializers import PlayerSerializer
from players.models import Player


def login(request):
    return redirect(reverse('social:begin', args=['yahoo-oauth2']))


def index(request):
    """Welcome home, baby
    """

    return render(request, 'index.html')


def league_dashboard(request):
    cache_key = 'player-ids'
    player_ids = cache.get(cache_key)

    if player_ids is None:
        print('setting in cache')
        player_ids = extract_cbs('dpf')
        cache.set(cache_key, player_ids, 12 * 60 * 60)

    # fetch pitchers whose ip >= 10
    players = (Player.objects
               .filter(key_cbs__in=player_ids,
                       ip__gte=5,
                       dra_minus__lte=100)
               .order_by('dra', 'cfip'))

    serialized_players = PlayerSerializer(players, many=True).data
    players_json = json.dumps(serialized_players)

    return render(request, 'reports/wire-pitching.html', {
        'players_json': players_json,
    })
