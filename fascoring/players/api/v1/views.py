from django.core.cache import cache

from rest_framework.generics import ListAPIView

from platform_backends.cbs import extract_players_for_league as extract_cbs
from players.models import Player

from .serializers import StatlineSerializer


class LeagueListView(ListAPIView):
    serializer_class = StatlineSerializer

    def get_queryset(self):
        cache_key = 'player-ids'
        player_ids = cache.get(cache_key)

        if player_ids is None:
            print('setting in cache')
            player_ids = extract_cbs()
            cache.set(cache_key, player_ids, 12 * 60 * 60)

        # fetch pitchers whose ip >= 10
        return (StatlineSerializer.objects
                   .filter(key_cbs__in=player_ids,
                           ip__gte=5,
                           dra_minus__lte=100)
                   .order_by('dra', 'cfip'))
