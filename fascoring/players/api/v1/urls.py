from django.conf.urls import url

from .views import LeagueListView


urlpatterns = [
    url(r'^$', LeagueListView.as_view(), name='list-league'),
]
