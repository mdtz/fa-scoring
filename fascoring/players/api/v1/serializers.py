from rest_framework.serializers import ModelSerializer, SerializerMethodField

from ...models import Player, Statline


class PlayerSerializer(ModelSerializer):
    class Meta:
        fields = ('name_full', 'primary_pos', 'dl_eligible',
                  'team_name', 'team_sos', 'team_upcoming',
                  'dl_eligible', 'dl_status', 'dl_reason',
                  'dl_last_updated', 'key_mlbam', )
        model = Player


class StatlineSerializer(ModelSerializer):
    player = PlayerSerializer()
    era_fip = SerializerMethodField()
    outs = SerializerMethodField()
    outs_per_game = SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = Statline

    def get_era_fip(self, obj):
        return round(obj.p_era - obj.p_fip, 2)

    def get_outs(self, obj):
        bits = str(obj.p_ip).split('.')

        if len(bits) == 1:
            whole, partial = bits[0], 0
        else:
            whole, partial = bits

        return int(whole) * 3 + int(partial)

    def get_outs_per_game(self, obj):
        outs = self.get_outs(obj)
        return outs / obj.p_g
