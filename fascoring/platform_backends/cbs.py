import re

from lxml import etree

import requests


BASE_URL = 'http://{league_name}.baseball.cbssports.com/print/xml/stats/stats-main/fa:SP:RP/as-restofseason:p/scoring/'

XML_RE = re.compile(r'(\<rows\>.*\<\/rows\>)', re.M | re.S)


def extract_leagues():
    raise NotImplementedError('CBS leagues cannot be extracted like this')


def extract_players_for_league(user_id, league_name):
    """Extracts CBS free agent pitching pool

    Args:
        league_name: CBS league name

    Returns:
        List of strings representing free agent CBS ids
    """

    league_url = BASE_URL.format(league_name=league_name)

    resp = requests.get(league_url)

    if not resp.status_code == requests.codes.ok:
        raise Exception('Unable to scrape backend: [{}] {}'
                        .format(resp.status_code, resp.reason))

    matches = XML_RE.search(resp.text)

    if matches is None:
        print(resp.text)
        raise Exception('CBS free agent data not found in blob')

    body = matches.group(1)
    body = body.replace('##', '#') # cbs cleanup
    doc = etree.fromstring(body)

    player_ids = set()

    for row in doc.xpath('/rows/row'):
        player_ids.add(row.attrib['player_id'])

    return player_ids
