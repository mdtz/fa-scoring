from users.models import PLATFORMS

from .cbs import (
    extract_players_for_league as cbs_extract_players_for_league)
from .yahoo import (
    extract_players_for_league as yahoo_extract_players_for_league)


def extract(team):
    if team.platform == PLATFORMS.yahoo:
        return yahoo_extract_players_for_league(team.user_id,
                                                team.platform_league_id)

    elif team.platform == PLATFORMS.cbs:
        return cbs_extract_players_for_league(team.user_id,
                                              team.platform_league_id)

    raise NotImplementedError('Backend {} is not implemented'
                              .format(team.platform))
