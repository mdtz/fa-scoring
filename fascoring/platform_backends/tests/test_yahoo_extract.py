import pytest

import responses

from platform_backends.yahoo import (
    extract_players_for_league,
    extract_leagues_and_teams,
    extract_stats_and_roster_positions)


TEST_YAHOO_LEAGUE_SETTINGS_URL = 'https://fantasysports.yahooapis.com/fantasy/v2/league/370.l.32452/settings'

@responses.activate
@pytest.mark.django_db
def test_yahoo_extract_teams_and_leagues(yahoo_user,
                                         yahoo_user_team,
                                         raw_yahoo_league_and_team_list,
                                         raw_yahoo_league_settings_response):
    """Tests fetching a given user's leagues

    Args:
        yahoo_user: Django user model fixture
        yahoo_user_team: Django user team model fixture
        raw_yahoo_league_list: Yahoo! league-team API XML response fixture
    """

    responses.add(
        responses.GET,
        'https://fantasysports.yahooapis.com/fantasy/v2/users;use_login=1/games;game_codes=mlb;seasons=2017/leagues/teams',
        status=200,
        body=raw_yahoo_league_and_team_list)

    responses.add(
        responses.GET,
        TEST_YAHOO_LEAGUE_SETTINGS_URL,
        status=200,
        body=raw_yahoo_league_settings_response)

    leagues_for_season = extract_leagues_and_teams(yahoo_user.id, 2017)

    assert len(leagues_for_season) == 1
    assert len(leagues_for_season[0]['teams']) == 1

    # ensure we've extracted the correct league
    league = leagues_for_season[0]
    team = league['teams'][0]

    assert (
        league['platform_league_id'] == yahoo_user_team.league.platform_id
    )
    assert (
        team['platform_team_id'] == yahoo_user_team.platform_id
    )

    assert league['name'] == yahoo_user_team.league.name
    assert team['name'] == yahoo_user_team.name


@responses.activate
@pytest.mark.django_db
def test_yahoo_extract(yahoo_user,
                       raw_yahoo_response,
                       yahoo_refresh_response):
    """Tests fetching players from Yahoo!'s API

    Args:
        yahoo_user: Django user model fixture
        raw_yahoo_response: Yahoo! player API XML response fixture
        yahoo_refresh_response: Mocked response from Yahoo!'s OAuth2 resource
    """

    # mock call to "all free agent pitchers for league" resource
    responses.add(
        responses.GET,
        'https://fantasysports.yahooapis.com/fantasy/v2/league/370.l.32452/players;status=FA;position=P;start=0',
        status=200,
        body=raw_yahoo_response)

    # mock call to oauth2 token refresh resource
    responses.add(responses.POST,
                  'https://api.login.yahoo.com/oauth2/get_token',
                  status=200,
                  body=yahoo_refresh_response)

    free_agent_ids = extract_players_for_league(user_id=1,
                                                league_key='370.l.32452',
                                                limit=25)

    # ensure that we weren't rejected
    assert free_agent_ids is not False
    assert len(free_agent_ids) == 25
    assert '5763' in free_agent_ids


@responses.activate
@pytest.mark.django_db
def test_yahoo_extract_stats_and_roster_positions(
        yahoo_user,
        raw_yahoo_league_settings_response):

    # mock call to league profile resource
    responses.add(responses.GET,
                  'https://fantasysports.yahooapis.com/fantasy/v2/league/370.l.32452/settings',
                  status=200,
                  body=raw_yahoo_league_settings_response)

    stats, positions = extract_stats_and_roster_positions(user=yahoo_user,
                                                          league_id='370.l.32452')

    assert len(stats['P']) == 8
    assert len(stats['B']) == 8
