"""
Extracts player ids from Yahoo league API
"""

import datetime
import json
import time

from django.contrib.auth.models import User

from lxml import etree

from social_django.utils import load_strategy

import requests


BASE_URL = 'https://fantasysports.yahooapis.com/fantasy/v2/league/{league_key}/players;status=FA;position=P'

NAMESPACES = {
    'xmlns:yahoo': 'http://www.yahooapis.com/v1/base.rng',
    'xmlns': 'http://fantasysports.yahooapis.com/fantasy/v2/base.rng',
}


def extract_stats_and_roster_positions(user, league_id):
    social = user.social_auth.filter(provider='yahoo-oauth2').first()

    time_alive = time.time() - social.extra_data['auth_time']
    if time_alive >= social.extra_data['expires']:
        strategy = load_strategy()
        social.refresh_token(strategy)

    # construct request
    access_token = social.extra_data['access_token']
    headers = {
        'authorization': 'bearer ' + access_token
    }

    resp = requests.get(
        'https://fantasysports.yahooapis.com/fantasy/v2/league/{league_id}/settings'
            .format(league_id=league_id),
        headers=headers)

    doc = etree.fromstring(resp.content)
    stats = {'P': [], 'B': []}
    positions = {'P': [], 'B': []}

    # extract stats from league settings
    stats_nodes = doc.findall(
        './xmlns:league/xmlns:settings/xmlns:stat_categories/xmlns:stats/xmlns:stat',
        namespaces=NAMESPACES)

    for node in stats_nodes:
        pos_type = node.find('./xmlns:position_type', namespaces=NAMESPACES).text
        pos = node.find('./xmlns:display_name', namespaces=NAMESPACES).text
        value = node.find('./xmlns:value', namespaces=NAMESPACES)
        stats[pos_type].append({
            'pos': pos,
            'value': 1 if not value else value.text,
        })

    # extract roster positions from league settings
    pos_nodes = doc.findall(
        './xmlns:league/xmlns:settings/xmlns:roster_positions/xmlns:roster_positions/xmlns:roster_position',
        namespaces=NAMESPACES)

    for node in pos_nodes:
        pos_type = node.find('./xmlns:position_type', namespaces=NAMESPACES).text
        pos = node.find('./xmlns:position', namespaces=NAMESPACES).text
        n = node.find('./xmlns:count', namespaces=NAMESPACES).text
        positions[pos_type].append({
            'pos': pos,
            'n': n,
        })

    return stats, positions


def extract_leagues_and_teams(user_id, season=None):
    """Extracts teams and leagues for a given user

    Args:
        user_id: database id for user
        season: Season year

    Returns:
        List of leagues with embedded list of teams
    """

    if season is None:
        season = datetime.date.today().year

    # fetch user
    user = User.objects.get(id=user_id)
    social = user.social_auth.filter(provider='yahoo-oauth2').first()

    time_alive = time.time() - social.extra_data['auth_time']
    if time_alive >= social.extra_data['expires']:
        strategy = load_strategy()
        social.refresh_token(strategy)

    # construct request
    access_token = social.extra_data['access_token']
    headers = {
        'authorization': 'bearer ' + access_token
    }

    resp = requests.get(
        'https://fantasysports.yahooapis.com/fantasy/v2/users;use_login=1/games;game_codes=mlb;seasons={season}/leagues/teams'
            .format(season=season),
        headers=headers)

    doc = etree.fromstring(bytes(resp.text, 'utf-8'))

    # extract players
    games_in_season = doc.xpath('//xmlns:game', namespaces=NAMESPACES)

    for game in games_in_season:
        leagues = [] # processed leagues
        league_nodes = game.findall('./xmlns:leagues/xmlns:league',
                                    namespaces=NAMESPACES)

        for league_node in league_nodes:
            key = league_node.find('./xmlns:league_key', namespaces=NAMESPACES).text
            name = league_node.find('./xmlns:name', namespaces=NAMESPACES).text
            url = league_node.find('./xmlns:url', namespaces=NAMESPACES).text
            scoring = league_node.find('./xmlns:scoring_type', namespaces=NAMESPACES).text

            # assemble league description
            league = {
                'platform_league_id': key,
                'name': name,
                'url': url,
                'scoring_type': scoring,
                'teams': [],
            }

            # extract stats and positions from league settings
            stats, positions = extract_stats_and_roster_positions(user, key)

            # find all teams this user belongs to in given league
            team_nodes = league_node.findall('./xmlns:teams/xmlns:team',
                                             namespaces=NAMESPACES)

            for team_node in team_nodes:
                key = team_node.find('./xmlns:team_key', namespaces=NAMESPACES).text
                name = team_node.find('./xmlns:name', namespaces=NAMESPACES).text
                url = team_node.find('./xmlns:url', namespaces=NAMESPACES).text

                team = {
                    'platform_team_id': key,
                    'name': name,
                    'url': url,
                }

                league['teams'].append(team)

            leagues.append(league)

        return leagues

    raise Exception('No teams found')


def extract_players_for_league(user_id, league_key, limit=500):
    """Extracts Yahoo! free agent pitching pool

    Args:
        user_id: database id for user
        league_key: Yahoo! league key

    Returns:
        List of strings representing free agent Yahoo! ids
    """

    user = User.objects.get(id=user_id)
    social = user.social_auth.filter(provider='yahoo-oauth2').first()

    if social is None:
        return False

    time_alive = time.time() - social.extra_data['auth_time']
    if time_alive >= social.extra_data['expires']:
        strategy = load_strategy()
        social.refresh_token(strategy)


    league_url = BASE_URL.format(league_key=league_key)

    headers = {
        'authorization': 'bearer ' + social.extra_data['access_token']
    }

    player_ids = set()

    # request up to 1000 players in steps of 25
    for step in range(0, limit, 25):
        url = league_url + ';start={}'.format(step)
        resp = requests.get(url, headers=headers)
        doc = etree.fromstring(bytes(resp.text, 'utf-8'))

        # extract players
        players = doc.xpath('//xmlns:player', namespaces=NAMESPACES)

        for player in players:
            player_id = (player
                         .find('./xmlns:player_id', namespaces=NAMESPACES)
                         .text)
            player_ids.add(player_id)

    return player_ids
