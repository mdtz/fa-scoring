from django.contrib import admin

from .models import League, Team


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'league', 'last_updated_at', 'state', )


@admin.register(League)
class LeagueAdmin(admin.ModelAdmin):
    list_display = ('name', 'platform', 'n_teams', 'last_updated_at', )
