import pytest

import responses

from users.models import PLATFORMS, League, Team
from users.pipeline import import_yahoo_leagues


TEST_YAHOO_TEAM_LIST_URL = 'https://fantasysports.yahooapis.com/fantasy/v2/users;use_login=1/games;game_codes=mlb;seasons=2017/leagues/teams'
TEST_YAHOO_LEAGUE_SETTINGS_URL = 'https://fantasysports.yahooapis.com/fantasy/v2/league/370.l.32452/settings'

@responses.activate
@pytest.mark.django_db
def test_import_yahoo_leagues_from_scratch(yahoo_user,
                                           raw_yahoo_league_and_team_list,
                                           raw_yahoo_league_settings_response):
    """Tests the Yahoo! league import pipeline

    Args:
        yahoo_user: Django user model fixture
        raw_yahoo_league_list: Yahoo! player API XML response fixture
    """

    responses.add(responses.GET,
                  TEST_YAHOO_TEAM_LIST_URL,
                  status=200,
                  body=raw_yahoo_league_and_team_list)

    responses.add(responses.GET,
                  TEST_YAHOO_LEAGUE_SETTINGS_URL,
                  status=200,
                  body=raw_yahoo_league_settings_response)

    qs = Team.objects.filter(user=yahoo_user,
                             platform_id='370.l.32452.t.6')

    # to start, we should have zero leagues
    assert qs.count() == 0

    # import leagues
    import_yahoo_leagues(yahoo_user)

    # after fixture-provided import, we should have one league
    assert qs.count() == 1

    # ... and it should be named "Flash Sideways"
    team = qs.first()
    assert team.league.name == 'Flash Sideways'
    assert team.name == 'Life on Earth'


@responses.activate
@pytest.mark.django_db
def test_import_yahoo_leagues_with_existing(yahoo_user,
                                            yahoo_user_team,
                                            raw_yahoo_league_list,
                                            raw_yahoo_league_settings_response):
    """Ensures that the Yahoo! import pipeline does not duplicate
    existing leagues

    Args:
        yahoo_user: Django user model fixture
        yahoo_user_team: Django user team model fixture
        raw_yahoo_league_list: Yahoo! player API XML response fixture
    """

    responses.add(responses.GET,
                  TEST_YAHOO_TEAM_LIST_URL,
                  status=200,
                  body=raw_yahoo_league_list)

    responses.add(responses.GET,
                  TEST_YAHOO_LEAGUE_SETTINGS_URL,
                  status=200,
                  body=raw_yahoo_league_settings_response)

    qs = Team.objects.filter(user=yahoo_user,
                             platform_id='370.l.32452.t.6')

    # to start, we should have zero leagues
    assert qs.count() == 1

    # import leagues
    import_yahoo_leagues(yahoo_user)

    # after fixture-provided import, we should have one league
    assert qs.count() == 1

    # ... and it should be named "Flash Sideways"
    team = qs.first()
    assert team.league.name == 'Flash Sideways'
    assert team.name == 'Life on Earth'
