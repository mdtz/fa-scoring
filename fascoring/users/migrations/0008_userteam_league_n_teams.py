# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-24 19:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20170614_2201'),
    ]

    operations = [
        migrations.AddField(
            model_name='userteam',
            name='league_n_teams',
            field=models.IntegerField(default=12),
        ),
    ]
