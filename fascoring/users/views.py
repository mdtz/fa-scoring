import json

from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.shortcuts import get_object_or_404, render

from players.api.v1.serializers import StatlineSerializer
from players.models import Player, Statline
from platform_backends import extract
from prospects.models import ProspectLine
from users.models import League, Team


@login_required
def add_league(request):
    pass


@login_required
def league_list(request):
    teams = Team.objects.select_related('league').filter(user=request.user)

    return render(request, 'teams/teams.html', {
        'teams': teams,
    })


# @login_required
def report_fa_pitching(request, team_id):
    team = get_object_or_404(Team, id=team_id)

    cache_key = 'player-ids:{}'.format(team_id)
    player_ids = cache.get(cache_key)

    if player_ids is None:
        player_ids = extract(team)
        cache.set(cache_key, player_ids, 12 * 60 * 60)

    player_key_filter = {
        'player__key_{}__in'.format(team.platform): player_ids,
    }

    players = (Statline.objects
               .select_related('player')
               .filter(p_ip__gte=5,
                       p_dra_minus__lte=100,
                       level='MLB',
                       **player_key_filter))

    serialized_players = StatlineSerializer(players, many=True).data
    players_json = json.dumps(serialized_players)

    return render(request, 'reports/wire-pitching.html', {
        'players_json': players_json,
    })


def report_fa_prospects(request, team_id):
    team = get_object_or_404(Team, id=team_id)

    cache_key = 'player-ids:{}'.format(team_id)
    player_ids = cache.get(cache_key)

    if player_ids is None:
        player_ids = extract(team)
        cache.set(cache_key, player_ids, 12 * 60 * 60)

    player_key_filter = {
        'player__key_{}__in'.format(team.platform): player_ids,
    }

    players = (ProspectLine.objects
               .select_related('player')
               .filter(**player_key_filter)
               .order_by('-rank_composite'))

    return render(request, 'reports/wire-prospects.html', {
        'players': players,
    })
