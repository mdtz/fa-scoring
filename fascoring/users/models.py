from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models

from fascoring.choices import Choices


PLATFORMS = Choices(
    ('yahoo', 'yahoo'),
    ('cbs', 'cbs'),
)

STATES = Choices(
    ('processing', 'processing'),
    ('ready', 'ready'),
    ('failed', 'failed'),
)

SCORING_TYPES = Choices(
    ('roto', 'Roto'),
    ('head', 'Head to head'),
)


class League(models.Model):
    # league platform details
    platform = models.CharField(max_length=12, choices=PLATFORMS)
    platform_id = models.CharField(max_length=120)

    # league details
    name = models.CharField(max_length=255, default='League')
    url = models.URLField(max_length=255, blank=True, null=True)
    scoring_type = models.CharField(max_length=12,
                                           choices=SCORING_TYPES,
                                           default=SCORING_TYPES.roto)
    n_teams = models.IntegerField(default=12)

    # league categories
    b_pos = ArrayField(models.CharField(max_length=3), null=True)
    b_stats = ArrayField(models.CharField(max_length=8),
                               blank=True, null=True)
    p_pos = ArrayField(models.CharField(max_length=3), null=True)
    p_pos = ArrayField(models.CharField(max_length=8),
                                blank=True, null=True)

    last_refreshed_at = models.DateTimeField(blank=True, null=True)
    last_updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} ({})'.format(self.platform, self.platform_league_id)


class Team(models.Model):
    league = models.ForeignKey(League, related_name='teams')
    user = models.ForeignKey(User, related_name='leagues')

    # team details
    platform_id = models.CharField(max_length=120)
    name = models.CharField(max_length=255, default='Team')
    url = models.URLField(max_length=255, blank=True, null=True)

    # is the user paying for this league
    is_enabled = models.BooleanField(default=False)

    # internal book-keeping
    state = models.CharField(max_length=24, choices=STATES,
                             default=STATES.ready)
    last_refreshed_at = models.DateTimeField(blank=True, null=True)
    last_updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} - {} / {}'.format(self.user.username,
                                     self.league.name,
                                     self.name)
