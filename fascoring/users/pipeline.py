"""
Pipeline step: import all leagues from Yahoo for a specific user
"""

import datetime

from django.shortcuts import redirect
from django.urls import reverse

import requests

from platform_backends.yahoo import (
    extract_leagues_and_teams as yahoo_extract_leagues_and_teams)
from users.models import PLATFORMS, League, Team


def import_yahoo_leagues(user=None, *a, **kw):
    """Fetches all leagues for the current season for given user
    """

    # limit league scope to current season
    # todo: this isn't reliable during the off-season
    season = datetime.date.today().year

    # fetch all of this user's leagues from yahoo
    raw_leagues = yahoo_extract_leagues_and_teams(user.id, season)

    for raw_league in raw_leagues:
        # create league if new
        lg_lkup = {
            'platform': PLATFORMS.yahoo,
            'platform_id': raw_league['platform_league_id'],
        }

        lg_defaults = {
            'name': raw_league['name'],
            'url': raw_league['url'],
            'scoring_type': raw_league['scoring_type'],
        }

        league, lgc = League.objects.update_or_create(**lg_lkup,
                                                      defaults=lg_defaults)

        for raw_team in raw_league['teams']:
            lkup = {
                'user': user,
                'league_id': league.id,
                'platform_id': raw_team['platform_team_id'],
            }

            defaults = {
                'name': raw_team['name'],
                'url': raw_team['url']
            }

            # create new team, or update existing with name and
            # (possibly updated) url
            team, c = Team.objects.update_or_create(**lkup, defaults=defaults)
