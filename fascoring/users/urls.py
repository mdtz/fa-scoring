from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.league_list, name='user-league-list'),

    url(r'^(?P<team_id>\d+)/reports/fa/pitching/$',
        views.report_fa_pitching,
        name='reports-fa-pitching'),

    url(r'^(?P<team_id>\d+)/reports/fa/prospects/$',
        views.report_fa_prospects,
        name='reports-fa-prospects'),
]
