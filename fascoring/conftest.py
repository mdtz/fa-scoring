import json
import os
import time

import pytest

from django.contrib.auth.models import User

import responses

from social_django.models import UserSocialAuth

from users.models import League, Team


BASE_DIR = os.path.dirname(__file__)
FIXTURES_DIR = os.path.join(BASE_DIR, os.pardir, 'tests', 'fixtures')

@pytest.fixture
def raw_response_cbs():
    fixture_path = os.path.join(FIXTURES_DIR, 'raw-response-cbs.xml')
    return open(fixture_path, 'r').read()


@pytest.fixture
def raw_yahoo_response():
    fixture_path = os.path.join(FIXTURES_DIR, 'raw-yahoo-response.xml')
    return open(fixture_path, 'r').read()


@pytest.fixture
def raw_yahoo_league_list():
    fixture_path = os.path.join(FIXTURES_DIR, 'raw-yahoo-league-list.xml')
    return open(fixture_path, 'r').read()


@pytest.fixture
def raw_yahoo_league_and_team_list():
    fixture_path = os.path.join(FIXTURES_DIR, 'raw-yahoo-team-list.xml')
    return open(fixture_path, 'r').read()


@pytest.fixture
def raw_yahoo_league_settings_response():
    fixture_path = os.path.join(FIXTURES_DIR, 'raw-yahoo-league-settings.xml')
    return open(fixture_path, 'r').read()


@pytest.fixture
def yahoo_refresh_response():
    return json.dumps({
        'access_token': '123',
        'refresh_token': '456',
    })


@pytest.fixture
def yahoo_user():
    user = User.objects.create(id=1, username='yahoo-user')

    extra_data = {
        'access_token': 'abc123',
        'auth_time': time.time(),
        'expires': 3600,
    }

    social = UserSocialAuth.objects.create(user=user,
                                           provider='yahoo-oauth2',
                                           uid='123',
                                           extra_data=extra_data)

    return user


@pytest.fixture
def yahoo_league():
    return League.objects.create(name='Flash Sideways',
                                 platform='yahoo',
                                 platform_id='370.l.32452')


@pytest.fixture
def yahoo_user_team(yahoo_league, yahoo_user):
    return Team.objects.create(user=yahoo_user,
                               league=yahoo_league,
                               name='Life on Earth',
                               platform_id='370.l.32452.t.6',
                               state='ready',
                               is_enabled=True)
