/**
 * Filter players in a league
 */

Date.prototype.addDays = function(days) {
  var d = new Date(this.valueOf() + days * 24 * 60 * 60 * 1000 );
  return d;
}

const today = new Date();

Vue.component('stat-list', {
  template: `
    <div class="pb-10">
      <h2>{{ title }}</h2>
      <div class="responsive">
        <pitcher-table :players="players" :stat="stat"></pitcher-table>
      </div>
    </div>
  `,

  props: ['title', 'players', 'stat']
});

Vue.component('pitcher-table', {
  template: `
    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Team</th>
          <th>Pos</th>
          <th>IP</th>
          <th :class="[{hilite: stat === 'p_outs_per_game'}]">O/G</th>
          <th>G/GS</th>
          <th :class="[{hilite: stat === 'p_wins'}]">W/L</th>
          <th :class="[{hilite: stat === 'p_qs'}]">QS</th>
          <th :class="[{hilite: stat === 'p_hld'}]">HLD</th>
          <th :class="[{hilite: stat === 'p_sv'}]">SV</th>
          <th>K-BB%</th>
          <th :class="[{hilite: stat === 'p_gmli'}]">gmLI</th>
          <th :class="[{hilite: stat === 'p_dra_minus'}]">DRA-</th>
          <th>cFIP</th>
          <th>FIP</th>
          <th :class="[{hilite: stat === 'p_era_fip'}]">ERA-FIP</th>
          <th>xwOBA+</th>
          <th>VH%</th>
          <th>EV</th>
        </tr>
      </thead>
      <tbody>
        <pitcher-detail
          v-for="player in players"
          :player="player"
          :stat="stat"
          key="player.key_mlbam">
        </pitcher-detail>
      </tbody>
    </table>
  `,

  props: ['players', 'stat']
})

Vue.component('pitcher-detail', {
  template: `
    <tr>
      <td>{{ player.player.name_full }}</td>
      <td>{{ player.player.team_name }}</td>
      <td>{{ player.player.primary_pos }}</td>
      <td>{{ player.p_ip }}</td>
      <td>{{ player.outs_per_game.toFixed(2) }}</td>
      <td>{{ player.p_g }}/{{ player.p_gs }}</td>
      <td :class="[{hilite: stat === 'wins'}]">{{ player.p_wins }}-{{ player.p_losses }}</td>
      <td :class="[{hilite: stat === 'qs'}]">{{ player.p_qs }}</td>
      <td :class="[{hilite: stat === 'hld'}]">{{ player.p_hld }}</td>
      <td :class="[{hilite: stat === 'sv'}]">{{ player.p_sv }}</td>
      <td>{{ player.p_so_bb }}%</td>
      <td :class="[{hilite: stat === 'gmli'}]">{{ player.p_gmli }}</td>
      <td :class="[{hilite: stat === 'dra_minus'}]">{{ player.p_dra_minus }}</td>
      <td>{{ player.p_cfip }}</td>
      <td>{{ player.p_fip }}</td>
      <td :class="[{hilite: stat === 'era_fip'}]">{{ (player.p_era - player.p_fip).toFixed(3) }}</td>
      <td>{{ player.p_x_xwoba_plus }}</td>
      <td>{{ player.p_x_vh }}%</td>
      <td>{{ player.p_x_avg_ev }}</td>
    </tr>
  `,

  props: ['player', 'stat']
});

new Vue({
  el: '#app',

  data: {
    players: window.__players__,
    includeInjuredInLists: true,
  },

  methods: {
    withholdAsInjured: function(player) {
      return this.includeInjuredInLists === false && player.player.dl_eligible === true;
    },

    top: function(type, n) {
      if (type == 'SP') {
        return this.starters.slice(0, n);
      } else if (type == 'RP') {
        return this.relievers.slice(0, n);
      } else {
        return _(this.players).filter(function(p) {
          return !this.withholdAsInjured(p);
        }.bind(this)).slice(0, n);
      }
    },

    topForStat: function(stat, n) {
      return (
        _.chain(this.players)
          .filter(function(p) {
            return p[stat] > 0 && !this.withholdAsInjured(p);
          }.bind(this))
          .sortBy(function(p) {
            return -1 * p[stat];
          })
          .value()
          .slice(0, n)
      );
    },

    longHaulRelievers: function(n) {
      return (
        _.chain(this.players)
          .filter(function(p) {
            return (
              p.outs_per_game >= 4
              && p.p_gmli >= 1.2
              && p.p_so_bb > 1.2
              && !this.withholdAsInjured(p)
            );
          }.bind(this))
          .sortBy(function(p) {
            return -1 * p.p_so_bb;
          })
          .value()
          .slice(0, n)
      );
    },

    startingSoon: function(n) {
      return _(this.players).filter(function(p) {
        if (!p.p_next_start_date) {
          return false;
        }

        const d = moment(p.p_next_start_date);
        const tomorrow = moment(new Date()).add(n, 'd');

        return d == p.player.name_full, d.isSame(tomorrow, 'date');
      })
    },

    topInjured: function(n) {
      return _(this.players).filter(function(p) {
        return p.player.dl_eligible === true;
      }).slice(0, n);
    }
  },

  computed: {
    starters: function(includeInjuredInLists) {
      return _(this.players).filter(function(p) {
        const withholdAsInjured = (
          this.includeInjuredInLists === false
          && p.player.dl_eligible === true);

        return p.player.primary_pos == 'SP' && !withholdAsInjured;
      }.bind(this));
    },

    relievers: function() {
      return _(this.players).filter(function(p) {
        const withholdAsInjured = (
          this.includeInjuredInLists === false
          && p.player.dl_eligible === true);

        return p.player.primary_pos == 'RP' && !withholdAsInjured;
      }.bind(this));
    },
  }
});
